package com.planet.ndkdynamicloadjar;

/**
 * Created by apple on 16/5/21.
 */
public class JNI {
    static {
        System.loadLibrary("JniTest");
    }

    public static native String getString();

    public static native String callJavaStaticMethod();

    public static native String callJavaInstaceMethod();
}
