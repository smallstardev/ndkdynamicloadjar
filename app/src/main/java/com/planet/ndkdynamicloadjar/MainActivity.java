package com.planet.ndkdynamicloadjar;

import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.interf.ILoader;

import java.io.File;

import dalvik.system.DexClassLoader;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

//        findViewById(R.id.button_jar).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Toast.makeText(MainActivity.this, "正在加载...", Toast.LENGTH_SHORT).show();
//                CallClassForC.loadJar(MainActivity.this);
//
//                Log.e(TAG, "jar load end...");
//            }
//        });

        findViewById(R.id.button_jni).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(MainActivity.this, "正在加载...", Toast.LENGTH_SHORT).show();


//                Toast.makeText(MainActivity.this, JNI.callJavaStaticMethod(), Toast.LENGTH_SHORT).show();
                Toast.makeText(MainActivity.this, JNI.callJavaInstaceMethod(), Toast.LENGTH_SHORT).show();
                Log.e(TAG, "jni load end...");
            }
        });
    }





}
