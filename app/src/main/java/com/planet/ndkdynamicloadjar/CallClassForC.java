package com.planet.ndkdynamicloadjar;

import android.app.Activity;
import android.content.Context;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.example.interf.ILoader;

import java.io.File;

import dalvik.system.DexClassLoader;

/**
 * Created by apple on 16/5/21.
 */
public class CallClassForC {
    private static final String FILE_NAME = "dynamicloader_dex.jar";
    private static final String JAR_CLASS_NAME = "com.example.loader.SayHelloLoader";


    protected static String callStaticMethod(String str) {
        return "C call Java static Class, parmas is: " + str;
    }

    protected String callInstanceMethod(String str) {
        loadJar(MyApplication.getContextObject());

        return str;
    }

    public static void loadJar(Context context) {

        File optimizedDexOutputPath = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator
                + FILE_NAME);// 外部路径
        File dexOutputDir = context.getDir("dex", 0);// 无法直接从外部路径加载.dex文件，需要指定APP内部路径作为缓存目录（.dex文件会被解压到此目录）
        DexClassLoader dexClassLoader = new DexClassLoader(optimizedDexOutputPath.getAbsolutePath(),
                dexOutputDir.getAbsolutePath(), null, context.getClassLoader());

        Class libProviderClazz = null;
        try {
            libProviderClazz = dexClassLoader.loadClass(JAR_CLASS_NAME);

            ILoader loader = (ILoader) libProviderClazz.newInstance();
            Toast.makeText(context, loader.sayHello(), Toast.LENGTH_SHORT).show();
        } catch (Exception exception) {
            // Handle exception gracefully here.
            exception.printStackTrace();
        }

    }
}
