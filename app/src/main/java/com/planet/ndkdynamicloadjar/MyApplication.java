package com.planet.ndkdynamicloadjar;

import android.app.Application;
import android.content.Context;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by apple on 16/5/22.
 */
public class MyApplication extends Application {
    private static final String TAG = "MyApplication";
    private static MyApplication sInstance;
    private static Context sContext;

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
        sContext = getApplicationContext();

    }

    public static Context getContextObject() {
        return sContext;
    }
}
