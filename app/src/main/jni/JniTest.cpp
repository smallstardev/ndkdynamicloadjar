#include "com_planet_ndkdynamicloadjar_JNI.h"
#include <android/log.h>


JNIEXPORT jstring JNICALL Java_com_planet_ndkdynamicloadjar_JNI_getString
        (JNIEnv *env, jclass) {
    return env->NewStringUTF("I am Jni...");
}

JNIEXPORT jstring JNICALL Java_com_planet_ndkdynamicloadjar_JNI_callJavaStaticMethod
        (JNIEnv *env, jclass cls) {

    jclass clazz = env->FindClass("com/planet/ndkdynamicloadjar/CallClassForC");
    if (clazz == 0) {
        return env->NewStringUTF("class not found");
    }

    jmethodID static_method_id = env->GetStaticMethodID(clazz, "callStaticMethod",
                                                        "(Ljava/lang/String;)Ljava/lang/String;");
    if (static_method_id == 0) {
        return env->NewStringUTF("找不到callStaticMethod这个静态方法。");
    }


    jstring str_arg = env->NewStringUTF("我是静态方法");
    env->CallStaticObjectMethod(clazz, static_method_id, str_arg);

    // 删除局部引用
    env->DeleteLocalRef(clazz);
    env->DeleteLocalRef(str_arg);

    return env->NewStringUTF("success");
}


JNIEXPORT jstring JNICALL Java_com_planet_ndkdynamicloadjar_JNI_callJavaInstaceMethod
        (JNIEnv *env, jclass cls) {


    jclass clazz = env->FindClass("com/planet/ndkdynamicloadjar/CallClassForC");
    if (clazz == 0) {
        return env->NewStringUTF("class not found");
    }

    jmethodID mid_construct = env->GetMethodID(clazz, "<init>", "()V");
    if (mid_construct == 0) {
        return env->NewStringUTF("找不到默认的构造方法");
    }

    jobject jobj = env->NewObject(clazz, mid_construct);


    jmethodID method_id = env->GetMethodID(clazz, "callInstanceMethod",
                                              "(Ljava/lang/String;)Ljava/lang/String;");
    if (method_id == 0) {
        return env->NewStringUTF("找不到方法：callInstanceMethod");
    }

    jstring str_arg = env->NewStringUTF("我是实例方法");
    jstring ret_string = (jstring) env->CallObjectMethod(jobj, method_id, str_arg);

    // 删除局部引用
    env->DeleteLocalRef(clazz);
    env->DeleteLocalRef(jobj);
    env->DeleteLocalRef(str_arg);

    return ret_string;
}
